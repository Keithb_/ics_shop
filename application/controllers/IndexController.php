<?php

class IndexController extends Zend_Controller_Action
{

    public function init()
    {
        $auth = Zend_Auth::getInstance();
        
        $auth->setStorage(new Zend_Auth_Storage_Session('SHOP'));
        
        if ($auth->hasIdentity())
        {
            
            $this->redirect('/icsshop');
            
        }
    }

    public function indexAction()
    {
       
    }


}

