<?php

class Icsshop_PurchasesController extends Zend_Controller_Action
{

    public function init()
    {
        $auth = Zend_Auth::getInstance();
        
        $auth->setStorage(new Zend_Auth_Storage_Session('SHOP'));
        
        //die(var_dump($auth->getStorage()->read()));
        
        if (!$auth->hasIdentity())
        {
            
            $this->redirect('/');
            
        }
    }

    public function indexAction()
    {
    }
    public function productslistAction(){
        
         $products = new Zend_Db_Table('products');

        $select = $products->select()->setIntegrityCheck(false);

        $query = $select->from(array('p' => 'products'), array(
                //products
            'p.products_id',
            'p.products_price',           
        ))
            ->join(array(
               'pd' => 'products_description'),
                'p.products_id=pd.products_id', array(
                 'pd.products_id',
                'pd.products_name'    
            ))->limit('100');
        $row = $products->fetchAll($query)->toArray();
                //This goes to the Productslist View to display the purchasable items
                $this->view->products = $row;
       
    }
    public function completepurchaseAction(){
        $productid = $this->_getParam('products_id',0);
       
        
        $products = new Zend_Db_Table('products');

        $select = $products->select()->setIntegrityCheck(false);

        $query = $select->from(array('p' => 'products'), array(
                //products
            'p.products_id',
            'p.products_price',
            'p.products_model'
        ))
            ->join(array(
               'pd' => 'products_description'),
                'p.products_id=pd.products_id', array(
                 'pd.products_id',
                'pd.products_name'    
            ))->where("p.products_id =$productid");
        $row = $products->fetchAll($query)->toArray();
                //This goes to the Productslist View to display the purchasable items
                $this->view->cart = $row;
    }
     public function successpurchaseAction(){
         //This action writes the order to the tables
          #$this->_helper->viewRenderer->setNoRender(true);
         $todaysdate = date("Y-m-d H:i:s");
           $params = $this->getAllParams();
           #die(print_r($params));
           if ($this->getRequest()->isPost()) {
//        $myData = $this->getRequest()->getPost();
           // die(print_r($params));
       $productid = $params['product'] = $params['prodid'];
       $customerid = $params['product'] = $params['customerid'];
       $productname = $params['product'] = $params['prodname'];
       $productmodel = $params['product'] = $params['prodmodel'];
       $productquantity = $params['product'] = $params['prodquantity'];
       $productprice = $params['product'] = $params['prodprice'];
       $total_price = $params['product'] = $params['total_price'];
       
       //We first insert to the orders table
      $db_orders = new Zend_Db_Table('orders');
        $datat1= array(
               'customers_id'=>$customerid,
               'customers_name'=>"Keith B ",
               'customers_company'=>"Declan Test Company",
                'date_purchased'=>$todaysdate
             );
           $db_orders->insert($datat1);
           //Any code below the json helper doesnt get executed
//           $this->_helper->json($data1);
           //Next we get the last inserted row frow the order table
           // as we need the orders_id to insert to the next table
        
       $select = $db_orders->select()->setIntegrityCheck(false);
        $query = $select->from(array('p' => 'orders'), array(
               
           'LAST_INSERT_ID(orders_id)'
        ))->order('p.orders_id DESC');
           $row = $db_orders->fetchAll($query)->toArray();
           foreach ($row[0] as $item){
  
           }
              $ordersid = $item;
              
           //Then we insert to the orders_products table
           $db_orders_products = new Zend_Db_Table('orders_products');
         $datat2= array(
               'orders_id'=>$ordersid,
               'products_id'=>$productid,
               'products_model'=>$productmodel,
               'products_name'=>$productname,
               'products_price'=>$productprice,
               'final_price'=>$total_price,
               'products_tax'=> '0.0000',
               'products_quantity'=>$productquantity,
               'vendors_id'=>'1'
             );
           $db_orders_products->insert($datat2);
           
            //Lastly we insert to the orders_total table
           $db_orders_total = new Zend_Db_Table('orders_total');
         $datat3= array(
               'orders_id'=>$ordersid,
               'title'=>$productid,
               'text'=>'<b style="font-size: 120%">&euro:'.$total_price.'</b>',
               'value'=>$total_price,
               'class'=>'ot_total',
               'sort_order'=>'800'
               
             );
           $db_orders_total->insert($datat3);
           //$this->_helper->json($data2);
          $urlOptions = array('controller' => 'purchases', 'action' => 'orderconfirmed', 'module' => 'icsshop');

            $this->_helper->redirector->gotoRoute($urlOptions);
           // $this->_helper->json('ok');
        }
    }
    public function orderconfirmedAction() {
        $productname = $this->_getParam('products_name',0);
         $this->view->prodname = $productname;
         
//         $finalprice = $this->_getParam('final_price',0);
//         $this->view->finalprice = $finalprice;
    }

}

