<?php

class Icsshop_AuthController extends Zend_Controller_Action
{

    public function indexAction()
    {
        $auth = Zend_Auth::getInstance();
        
        $auth->setStorage(new Zend_Auth_Storage_Session('SHOP'));
        
        if ($auth->hasIdentity())
        {
            
            $this->redirect('/icsshop/purchases/productslist');
            
        } else {
            
           die("Not Authorized!!!");
           
        }
        
    }
    
    public function dotnetLoginAction()
    {
        
        $customerID = (int) $this->getParam('cid', 0);
        $email = $this->getParam('email', 'default');
        
        $auth = Zend_Auth::getInstance();
                
        $auth->setStorage(new Zend_Auth_Storage_Session('SHOP'));
        $namespace = new Zend_Session_Namespace('SHOP');
        $namespace->setExpirationSeconds(3600); // 1 hour
        
        $authAdapter = $this->getAuthAdapter();
        
        $authAdapter->setIdentity($email)
                    ->setCredential($customerID);
        
        $result = $auth->authenticate($authAdapter);
        
        if ($result->isValid())
        {
            $identity = $authAdapter->getResultRowObject();

            $authStorage = $auth->getStorage();
            
            $authStorage->write($identity);

            $this->redirect('/icsshop');
        }
        else
        {
            die("Login attempt failed");
        }
        
    }
    
    public function logoutAction()
    {
        Zend_Auth::getInstance()->clearIdentity();
        
        $namespace = new Zend_Session_Namespace('SHOP');
        $namespace->unsetAll();
        
        Zend_Session::destroy( true );
        
        $this->redirect('/');
    }
    
    private function getAuthAdapter()
    {
        $authAdapter = new Zend_Auth_Adapter_DbTable(Zend_Db_Table::getDefaultAdapter());
        $authAdapter->setTableName('customers')
                    ->setIdentityColumn('customers_email_address')
                    ->setCredentialColumn('customers_ics_id');

        return $authAdapter;
    }
    
}