<?php

class Icsshop_SalesController extends Zend_Controller_Action
{

    public function init()
    {
        $auth = Zend_Auth::getInstance();
        
        $auth->setStorage(new Zend_Auth_Storage_Session('SHOP'));
        
        if (!$auth->hasIdentity())
        {
            
            $this->redirect('/');
            
        }
    }

    public function indexAction()
    {
       
    }
    
    public function salesinformationAction()
    {
        
       $order_table = new Zend_Db_Table('orders');
        $select = $order_table->select()->setIntegrityCheck(false);
        $query = $select->from(array('o' => 'orders'), array(
                //Order Info
            'o.customers_id',
            'COUNT(DISTINCT o.customers_id) as result_count'
        ))
          ->join(array(
               'op' => 'orders_products'),
                'o.orders_id=op.orders_id', array(
                'op.orders_id'
            ))->limit('1000');
        
        $row = $order_table->fetchAll($query)->toArray();
                //This goes to the Salesview and displays the 
                 $this->view->amntcustomers = $row;
                
         $orders_products_table = new Zend_Db_Table('orders_products');
        $select = $orders_products_table->select()->setIntegrityCheck(false);
        $query = $select->from(array('op' => 'orders_products'), array(
                //Order Info
            'op.products_name',
            'COUNT(*) as common'
        ))->group('op.products_name')->order('common DESC')->limit('1');
//        die($select->assemble());
        $result = $orders_products_table->fetchAll($query)->toArray();
        //die(print_r($result));
      $this->view->common = $result;
      
//       $ot = new Zend_Db_Table('orders');
//      $select = $ot->select()->setIntegrityCheck(false);
//        $query = $select->from(array('o' => 'orders'), array(
//                //Order Info
//            'o.orders_id',
//            'COUNT(*)'
//        ))->where("YEAR(date_purchased)='2018'")->group('MONTH(date_purchased)');
//        $months =$ot->fetchAll($query)->toArray();
//        //For the graph
//       $this->view->monthsales = $months; 
       //End of working figures
     $arraygraphdata= $this->graphdata();
      $items18 = array();
      $items17 = array();
      $items16 = array();
 	list($graph2018,$graph2017,$graph2016)=$arraygraphdata;
         foreach($graph2018 as $months18){
              $items18[]= $months18['COUNT(*)'];
         }
         foreach($graph2017 as $months17){
              $items17[]= $months17['COUNT(*)'];
         }
         foreach($graph2016 as $months16){
              $items16[]= $months16['COUNT(*)'];
         }
        $this->view->sales18 =$items18;
        $this->view->sales17 =$items17;
        $this->view->sales16 =$items16;
	//Sales figure query 
      $params = $this->getAllParams();
           if ($this->getRequest()->isPost()) {
       $myData = $this->getRequest()->getPost();

           }
      $ot = new Zend_Db_Table('orders');
        $select = $ot->select()->setIntegrityCheck(false);
        $query = $select->from(array('o' => 'orders'), array(
                //Order Info
            'o.orders_id',
            'o.date_purchased'
        ))
          ->join(array(
               'ot' => 'orders_total'),
                'o.orders_id=ot.orders_id', array(
                'ot.orders_id',
                'ot.value', 
                'SUM(ot.value)'
            ))->where("(ot.class = 'ot_total') AND o.date_purchased BETWEEN '2017-01-01 00:00:00' AND '2018-12-31 00:00:00'");
        //die($select->assemble());
        $return = $ot->fetchAll($query)->toArray();
                //This goes to the Salesview and displays the  sales value from Jan2017- the end of 2018
//                 $this->view->svalue = $return;
               foreach ($return as $all_time_sales_amnt):
                  $format_all_time_sales_amnt = number_format($all_time_sales_amnt['SUM(ot.value)'], 2);
               endforeach;
               $this->view->all_time_sales_value = $format_all_time_sales_amnt;
               $this->view->salesarray = $this->salesfilters($format_all_time_sales_amnt);
              // die(print_r($this->view->salesarray));
      //End
	  
    
                 
           }
      //End
           
                 
  
    private function salesfilters($format_all_time_sales_amnt){
        //Assinging the variables
        $oneweekago = new DateTime('-1 week'); $oneweekago = $oneweekago->format('Y-m-d H:i:s');
        $first_day_this_month = date('Y-m-01 H:i:s');
        $todaysdate = date("Y-m-d H:i:s");
        $first_day_of_this_year = new DateTime('first day of January');
        $first_day_of_this_year = $first_day_of_this_year->format('Y-m-d H:i:s');
        
         
        //Sales this week
        $ot = new Zend_Db_Table('orders');
        $select = $ot->select()->setIntegrityCheck(false);
        $query = $select->from(array('o' => 'orders'), array(
                //Order Info
            'o.orders_id',
            'o.date_purchased'
        ))
          ->join(array(
               'ot' => 'orders_total'),
                'o.orders_id=ot.orders_id', array(
                'ot.orders_id',
                'ot.value', 
                'SUM(ot.value)'
            ))->where("(ot.class = 'ot_total') AND o.date_purchased BETWEEN '$oneweekago' AND '$todaysdate'");
        //die($select->assemble());
        $return = $ot->fetchAll($query)->toArray();
        foreach ($return as $thisweeksalesamnt):
                  $format_this_weeks_sales_amnt = number_format($thisweeksalesamnt['SUM(ot.value)'], 2);
               endforeach;
        //End of Sales this week
        
        //Sales this month
         $ot = new Zend_Db_Table('orders');
        $select = $ot->select()->setIntegrityCheck(false);
        $query = $select->from(array('o' => 'orders'), array(
                //Order Info
            'o.orders_id',
            'o.date_purchased'
        ))
          ->join(array(
               'ot' => 'orders_total'),
                'o.orders_id=ot.orders_id', array(
                'ot.orders_id',
                'ot.value', 
                'SUM(ot.value)'
            ))->where("(ot.class = 'ot_total') AND o.date_purchased BETWEEN '$first_day_this_month' AND '$todaysdate'");
        //die($select->assemble());
        $return = $ot->fetchAll($query)->toArray();
        foreach ($return as $thismonthsalesamnt):
                  $format_this_months_sales_amnt = number_format($thismonthsalesamnt['SUM(ot.value)'], 2);
               endforeach;
        //End of Sales this month
        
                //Sales this year
         $ot = new Zend_Db_Table('orders');
        $select = $ot->select()->setIntegrityCheck(false);
        $query = $select->from(array('o' => 'orders'), array(
                //Order Info
            'o.orders_id',
            'o.date_purchased'
        ))
          ->join(array(
               'ot' => 'orders_total'),
                'o.orders_id=ot.orders_id', array(
                'ot.orders_id',
                'ot.value', 
                'SUM(ot.value)'
            ))->where("(ot.class = 'ot_total') AND o.date_purchased BETWEEN '$first_day_of_this_year' AND '$todaysdate'");
        //die($select->assemble());
        $return = $ot->fetchAll($query)->toArray();
        foreach ($return as $thisyearsalesamnt):
                  $format_this_years_sales_amnt = number_format($thisyearsalesamnt['SUM(ot.value)'], 2);
               endforeach;
        //End of Sales this year
               
        $sales_figures=[
          'Sales All Time' => $format_all_time_sales_amnt,
            'Sales This Year' => $format_this_years_sales_amnt,
            'Sales This Month' => $format_this_months_sales_amnt,
            'Sales Over The last 7 Days' => $format_this_weeks_sales_amnt
                ];
        return $sales_figures;
 
    }//End of salesfigures private function
    
    private function graphdata() {
       //2018
      $ot = new Zend_Db_Table('orders');
      $select = $ot->select()->setIntegrityCheck(false);
        $query = $select->from(array('o' => 'orders'), array(
                //Order Info
            'o.orders_id',
            'COUNT(*)'
        ))->where("YEAR(date_purchased)='2018'")->group('MONTH(date_purchased)');
        $months18 =$ot->fetchAll($query)->toArray();
        //For the graph
        
  
   
   //2017
      $ot = new Zend_Db_Table('orders');
      $select = $ot->select()->setIntegrityCheck(false);
        $query = $select->from(array('o' => 'orders'), array(
                //Order Info
            'o.orders_id',
            'COUNT(*)'
        ))->where("YEAR(date_purchased)='2017'")->group('MONTH(date_purchased)');
        $months17 =$ot->fetchAll($query)->toArray();
        //For the graph
        
   
   
   //2016
      $ot = new Zend_Db_Table('orders');
      $select = $ot->select()->setIntegrityCheck(false);
        $query = $select->from(array('o' => 'orders'), array(
                //Order Info
            'o.orders_id',
            'COUNT(*)'
        ))->where("YEAR(date_purchased)='2016'")->group('MONTH(date_purchased)');
        $months16 =$ot->fetchAll($query)->toArray();
        //For the graph
        $yearlygraphfigures =array($months18,$months17,$months16);
   return  $yearlygraphfigures; 
   
    }
   
}