<?php

class Icsshop_CustomerController extends Zend_Controller_Action
{

    public function init()
    {
        $auth = Zend_Auth::getInstance();
        
        $auth->setStorage(new Zend_Auth_Storage_Session('SHOP'));
        
        if (!$auth->hasIdentity())
        {
            
            $this->redirect('/');
            
        }
    }

    public function indexAction()
    {
       
    }
    
    public function customertableAction() {
        
         $order_table = new Zend_Db_Table('orders');
        $select = $order_table->select()->setIntegrityCheck(false);
        $query = $select->from(array('o' => 'orders'), array(
                //Order Info
            'o.orders_id',
            'o.customers_id',
            'o.customers_name',
            'o.customers_company',
            'o.customers_email_address',
            'o.customers_telephone'
        ))
          ->join(array(
               'c' => 'customers'),
                'o.customers_id=c.customers_id', array(
                'c.customers_account_approval'
            ))->limit('1000');
        
        $row = $order_table->fetchAll($query)->toArray();
        //Will be using customers in the view to loop through
                $this->view->customers = $row;
       
    }
    public function customerorderAction() {
        $custid = $this->_getParam('customers_id',0);
        $name = $this->_getParam('name',0);
        $customerorders = new Zend_Db_Table('orders_products');
        $select = $customerorders->select()->setIntegrityCheck(false);
        $query = $select->from(array('op' => 'orders_products'), array(
            'op.orders_id',
            'op.products_name',
            'op.final_price',
            'op.products_quantity'
            )) 
             ->join(array(
               'ot' => 'orders_total'),
                'op.orders_id=ot.orders_id', array(
                'ot.text',
                'ot.class'
            ))
            ->join(array(
               'ord' => 'orders'),
                'ot.orders_id=ord.orders_id', array(
                'ord.customers_id',
                 'ord.customers_company',
                 'ord.date_purchased'
            ))
          ->where("ord.customers_id =$custid AND ot.class = 'ot_total'");
        
        $row = $customerorders->fetchAll($query)->toArray();
        //Will be using customers in the view to loop through
                $this->view->customerorders = $row;
                $this->view->company =$name;
                #$this->view->companyname= $row['customers_company'];
    }
}