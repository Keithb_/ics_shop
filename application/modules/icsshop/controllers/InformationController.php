<?php

class Icsshop_InformationController extends Zend_Controller_Action
{

    public function init()
    {
        $auth = Zend_Auth::getInstance();
        
        $auth->setStorage(new Zend_Auth_Storage_Session('SHOP'));
        
        if (!$auth->hasIdentity())
        {
            
            $this->redirect('/');
            
        }
    }

    public function indexAction()
    {
       
    }
    public function orderinfoAction(){
         $id = $this->_getParam('orders_id',0);
         $prod_name = $this->_getParam('productname',0);
         $orders_table = new Zend_Db_Table('orders');
         $select = $orders_table->select()->from('orders')->where('orders_id = ?',$id);
        $result = $orders_table->fetchAll($select);
        $this->view->moreOrderInfo =$result;
        $this->view->prodname = $prod_name;
        /*
        $id = $this->_getParam('orders_id',0);
        $orders = new Zend_Db_Table('orders');
        $select = $orders->select()->from('orders')->where('orders_id = ?',$id);
        $result =$orders->fetchAll($select);
        $this->view->moreOrderInfo =$result;


       //This goes to the Orderinfo View to display the Order from the relevant order_id
       
                $orders_products_table = new Zend_Db_Table('orders_products');
                $selectFromPo = $orders_products_table->select()->from('orders_products')->where('orders_id = ?',$id);
                $return=$orders_products_table->fetchAll($selectFromPo)->toArray();
                    $this->view->products = $return;
//                    die(print_r($this->view->products));
         */
        
    
        
    }
}