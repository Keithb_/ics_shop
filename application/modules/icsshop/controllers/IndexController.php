<?php

class Icsshop_IndexController extends Zend_Controller_Action
{

    public function init()
    {
        $auth = Zend_Auth::getInstance();
        
        $auth->setStorage(new Zend_Auth_Storage_Session('SHOP'));
        
        if (!$auth->hasIdentity())
        {
            
            $this->redirect('/');
            
        }
    }

    public function indexAction()
    {
            $orders_products = new Zend_Db_Table('orders_products');

        $select = $orders_products->select()->setIntegrityCheck(false);

        $query = $select->from(array('op' => 'orders_products'), array(
                //Order Info
            'op.orders_id',
            'op.products_name',
            'op.final_price',
            'op.products_quantity'
        ))
            ->join(array(
               'ot' => 'orders_total'),
                'op.orders_id=ot.orders_id', array(
                'ot.text',
                'ot.class'             
            ))->where("ot.class = 'ot_total'")
               ->limit('1000');
        $row = $orders_products->fetchAll($query)->toArray();
                //This goes to the Orderinfo View to display the customer Info
                $this->view->orderinfo = $row;  
    }
   
    
    

}

